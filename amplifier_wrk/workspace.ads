<ADSWorkspace Revision="54" Version="100">
    <Workspace Name="">
        <Library Name="ads_standard_layers" />
        <Library Name="adstechlib" />
        <Library Name="ads_schematic_layers" />
        <Library Name="empro_standard_layers" />
        <Library Name="ads_standard_layers_ic" />
        <Library Name="ads_schematic_layers_ic" />
        <Library Name="ads_schematic_ports_ic" />
        <Library Name="ads_rflib" />
        <Library Name="ads_sources" />
        <Library Name="ads_simulation" />
        <Library Name="ads_tlines" />
        <Library Name="ads_bondwires" />
        <Library Name="ads_datacmps" />
        <Library Name="ads_behavioral" />
        <Library Name="ads_textfonts" />
        <Library Name="ads_common_cmps" />
        <Library Name="ads_designs" />
        <Library Name="ads_verification_test_bench" />
        <Preferences Name="layout.prf" />
        <Preferences Name="schematic.prf" />
        <LibraryDefs Name="lib.defs" />
        <ConfigFile Name="de_sim.cfg" />
        <ConfigFile Name="hpeesofsim.cfg" />
        <ConfigFile Name="dds.cfg" />
        <Preferences Name="ads_rflib_lay.prf" />
        <Dataset Name="S_param_simulation.ds" />
        <Data_Display Name="S_param_simulation.dds" />
        <Library Name="amplifier_lib" />
        <Preferences Name="amplifier_lib_lay.prf" />
        <LibAel Name="amplifier_lib:boot.ael" />
        <Cell Name="amplifier_lib:S_param_simulation" />
        <Log Name="search_history.log" />
        <Folder Name="symbols">
            <Cell Name="amplifier_lib:capacitorMurata_symbol" />
            <Cell Name="amplifier_lib:inductorCoilcraft_symbol" />
            <Cell Name="amplifier_lib:bfp520_symbol" />
        </Folder>
        <Folder Name="layouts">
            <Cell Name="amplifier_lib:connector_layout" />
        </Folder>
        <Log Name="netlist.log" />
    </Workspace>
</ADSWorkspace>
